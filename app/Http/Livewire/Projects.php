<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Project;
use App\Models\Technologys;

class Projects extends Component
{
	public $projects, $technology_lists, $title, $description, $technology, $project_id;
	public$isOpen = 0;

    public function render() {
    	$this->projects = Project::all();
    	$this->technology_lists = Technologys::all();
        return view('livewire.projects');
    }

    public function createProject() {
        $this->resetProjectInputFields();
        $this->openProjectFormModal();
    }

    public function openProjectFormModal() {
		$this->isOpen = true;
    }
    public function closeProjectModal()
    {
        $this->isOpen = false;
    }
    public function resetProjectInputFields() {
    	$this->title 		= '';
        $this->description 	= '';
        $this->technology 	= '';
        $this->project_id 	= '';
    }
    public function storeProject() {
		$this->validate([
            'technology' 	=> 'required',
            'title' 		=> 'required',
            'description' 	=> 'required'
        ]);
   
        Project::updateOrCreate( [ 'id' => $this->project_id ], 
        	[
	            'technology' 	=> $this->technology,
	            'title' 		=> $this->title,
	            'description' 	=> $this->description
	        ]
	    );
  
        session()->flash(
        	'message', 
        	$this->project_id ? 'Project Updated Successfully.' : 'Project Created Successfully.'
        );
  
        $this->closeProjectModal();
        $this->resetProjectInputFields();
    }

    public function editProject( $id ) {
        $project = Project::findOrFail($id);
        $this->project_id    = $id;
        $this->technology    = $project->technology;
        $this->title         = $project->title;
        $this->description   = $project->description;
    
        $this->openProjectFormModal();
    }

    public function deleteProject( $id ) {
        Project::find($id)->delete();
        session()->flash('message', 'Project Deleted Successfully.');
    }
}
