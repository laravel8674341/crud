<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Technologys;

class Technology extends Component
{
	public $title, $description, $technology_id;
    public $isOpen = 0;
    use WithPagination;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function render()
    {
        $technologys =  Technologys::paginate( 5 );
        return view('livewire.technology', [
            'technology' => $technologys
        ]);
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->isOpen = true;
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->title = '';
        $this->description = '';
        $this->technology_id = '';
    }
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
   
        Technologys::updateOrCreate(['id' => $this->technology_id], [
            'title' => $this->title,
            'description' => $this->description
        ]);
  
        session()->flash('message', $this->technology_id ? 'Technology Updated Successfully.' : 'Technology Created Successfully.');
  
        $this->closeModal();
        $this->resetInputFields();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $post = Technologys::findOrFail($id);
        $this->technology_id = $id;
        $this->title = $post->title;
        $this->description = $post->description;
    
        $this->openModal();
    }
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Technologys::find($id)->delete();
        session()->flash('message', 'Technology Deleted Successfully.');
    }
}
