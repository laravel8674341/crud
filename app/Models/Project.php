<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'technology', 'title', 'description'
    ];
    
    public function projectTechnology() {
        return $this->belongsTo('App\Models\Technologys','technology');
    }
}
