<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Technology') }}
    </h2>
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
            @if (session()->has('message'))
                <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3" role="alert">
                  <div class="flex">
                    <div>
                      <p class="text-sm">{{ session('message') }}</p>
                    </div>
                  </div>
                </div>
                <br>
            @endif
            <button wire:click="createProject()" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 transition ease-in-out duration-150">Create New Project</button>
            <br><br>
            @if( $isOpen )
                @include( 'livewire.create_project' )
            @endif
            <table class="table-fixed w-full">
                <thead>
                    <tr class="bg-gray-100">
                        <th class="border px-4 py-2 text-left w-20">No.</th>
                        <th class="border px-4 py-2 text-left">Project Title</th>
                        <th class="border px-4 py-2 text-left">Technology</th>
                        <th class="border px-4 py-2 text-left">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($projects as $project)
                    <tr>
                        <td class="border px-4 py-2">{{ $project->id }}</td>
                        <td class="border px-4 py-2">{{ $project->title }}</td>
                        <td class="border px-4 py-2">{{ $project->projectTechnology->title }}</td>
                        <td class="border px-4 py-2">
                        <button wire:click="editProject({{ $project->id }})" class="bg-blue-500 hover:bg-blue-700 font-bold py-2 px-4 rounded">Edit</button>
                            <button wire:click="deleteProject({{ $project->id }})" class="bg-red-500 hover:bg-red-700 font-bold py-2 px-4 rounded">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>